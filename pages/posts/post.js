var postsData = require('../../data/posts-data.js')

Page({
  data: {
    //小程序总是会读取data对象来做数据绑定，这个动作我们称为动作A
    // 而这个动作A的执行，是在onLoad函数执行之后发生的
  },
  onLoad: function () {

    // this.data.postList = postsData.postList
    this.setData({
       postList:postsData.postList
      });
  },

  /* 点击进入详情页面 */
  onPostTap: function (event) {
    var postId = event.currentTarget.dataset.postid;
    console.log("onPostTap on post id is " + postId);
    wx.navigateTo({
      url: "post-detail/post-detail?id=" + postId
    })
  },
  /* 点击轮播图进入详情页面 */
  onSwiperTap: function (event) {
    // target 和currentTarget
    // target指的是当前点击的组件 和currentTarget 指的是事件捕获的组件
    // target这里指的是image，而currentTarget指的是swiper
    var postId = event.target.dataset.postid;
    console.log("onSwiperTap on post id is " + postId);
    wx.navigateTo({
      url: "post-detail/post-detail?id=" + postId
    })
  },

     /**
   *设置转发
   */
  onShareAppMessage: function () {
    return {
      title: '分享肉萌萌世界,让养殖多肉变得更简单',
      desc: '多肉世界,多肉植物的天堂',
      path: '/post-item/post-item-template'

    }
  },
})