var sell_data = require('../../data/sell-data.js')
Page({
  /**
   * 页面的初始数据
   */
  data: {

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(option) {
    var sellData = sell_data.sellList[0];
    this.setData({
      sellData: sellData
    })

  },

  /**
   *设置转发
   */
  onShareAppMessage: function() {
    return {
      title: '购买请添加微信:18345823879',
      desc: '东北土特产,是你走亲访友不错的选择',
      path: '/post-item/post-item-template'

    }
  },



})