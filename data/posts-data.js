var local_database = [{

    /* 开发团队 */
    date: "2019-02-19",
    title: "开发团队",
    imgSrc: "/images/post/develop.jpg",
    avatar: "/images/avatar/tx.png",
    content: "晨港飞燕是一群工作之余的编程开发人员组织的团队,主要对新技术的探讨和实践!",
    reading: "112",
    collection: "96",
    headImgSrc: "/images/post/develop.jpg",
    author: "晨港飞燕",
    dateTime: "2019-02-19",
    detail: [{
      "header": "上海团队",
      "name": "姓名:杨建飞",
      "address": "地址:上海市徐汇区漕宝路124号",
      "phone": "手机:13641786571",
      "header1": "天津团队",
      "name1": "姓名:刘彦登",
      "address1": "地址:天津市滨海新区闸北路20号",
      "phone1": "手机:18201202293",
      "header2": "东北团队",
      "name2": "姓名:柴乃文",
      "address2": "地址:双鸭山市委通江街南",
      "phone2": "手机:18345823879",
      "flag": '0'
    }],
    postId: 0,
    music: {
      url: "http://www.ytmp3.cn/down/57222.mp3",
      title: "Ijiigee Gomdooh Uchirgui-Ijiigee Gomdooh Uchirgui-Gurvan Buduun",
      coverImg: "/images/music/rmmmusic.png"
    }
  },

  /* 合作加盟 */

  {
    title: "合作加盟",
    content: "欢迎在工作之余有自己的支配时间的同志对项目进行完善,一起探讨和专研新技术",
    imgSrc: "/images/post/join.jpg",
    reading: 62,
    detail: [{
        "cond1": "1.工作之余有自己的支配时间",
        "cond2": "2.热爱技术,喜欢探讨和专研新技术",
        "cond3": "3.扎实的Java,Javascript,HTML5+CSS3等基础",
        "cond4": "4.熟悉主流框架技术,如thymeleaf,vue,springboot,elasticsearch等",
        "cond5": "5.熟悉分布式相关技术,如Nginx,SpringCloud等",
        "cond6": "6.熟练使用H2,MySql等关系型数据库",
        "cond7": "7.熟练使用git,idea,STS等开发工具",
        "cond8": "8.熟悉linux操作",
        "cond9": "9.良好的沟通能力",
        "cond10": "10.良好的英语水平",
        "cond11": "无收益",
        "flag": '1'
      }

    ],
    collection: 92,
    dateTime: "2019-02-19",
    headImgSrc: "/images/post/join.jpg",
    author: "晨港飞燕",
    date: "2019-02-19",
    avatar: "/images/avatar/tx.png",
    postId: 1,
    music: {
      url: "http://www.ytmp3.cn/down/56505.mp3",
      title: "唱一首情歌-冷漠&龙梅子",
      coverImg: "/images/music/rmmmusic.png"
    }
  },

  /* 团队成员 */
  {
    //按住alt + shift + F 可以格式化代码样式
    title: "团队成员",
    content: "团队成员不分地域,分布在全国各地,分工不同,进行远程协调!",
    detail: [{
      "name": "柴乃文",
      "intro": "销售人员(各类东北特产)微信:18345823879",
      "photo": "/images/post/cnw.jpg",
      "name1": "刘彦登",
      "intro1": "开发人员",
      "photo1": "/images/post/headdefault.png",
      "name2": "杨建飞",
      "intro2": "开发人员",
      "photo2": "/images/post/headdefault.png",
      "flag": '2'
    }],
    imgSrc: "/images/post/team.jpg",
    headImgSrc: "/images/post/team.jpg",
    reading: 62,
    collection: 92,
    author: "晨港飞燕",
    date: "2019-02-19",
    dateTime: "2019-02-19",
    avatar: "/images/avatar/tx.png",
    postId: 2,
    music: {
      url: "http://www.ytmp3.cn/down/50166.mp3",
      title: "Conquest Of Paradise-Vangelis(登推荐)",
      coverImg: "/images/music/rmmmusic.png"
    }
  }

]

module.exports = {
  postList: local_database
}